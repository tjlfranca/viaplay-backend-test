// process.env.API_SECRET = "API_SECRET";

module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  globals: {
    "ts-jest": {
      tsConfig: "./tsconfig.json",
    },
  },
  coveragePathIgnorePatterns: ["<rootDir>/node_modules/", ".mock.ts$"],
};
