import express, { Application } from "express";
import helmet from "helmet";

import { trailerController } from "./controllers/trailer.controller";

const app: Application = express();

app.use(helmet()).use(express.json());

app.get("/", (req, res: any) => res.json({ message: "Viaplay Backend test" }));
app.use("/trailer", trailerController());

app.use((err, req, res, _next) => res.status(err.httpCode || 500).json({ error: err.message }));

export { app };
