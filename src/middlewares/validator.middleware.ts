import { NextFunction, Request, Response } from "express";
import { Schema } from "joi";

const validateMiddleware = (path: string) => (schema: Schema) => (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const { error } = schema.validate(req[path]);

  if (error) {
    return res.status(400).json({ error });
  }

  next();
};

export const validateBody = validateMiddleware("body");
export const validateQs = validateMiddleware("query");
