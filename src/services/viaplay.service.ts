import axios from "axios";

import { Exception } from "../exception";
import { logger } from "../logger";

export interface IMDBDetails {
  id?: string;
  rating?: string;
  votes?: string;
  url?: string;
}

const extractImdbDetails = (data): IMDBDetails => {
  const blocks = data?._embedded["viaplay:blocks"];
  if (blocks && blocks[0]) {
    return blocks[0]?._embedded["viaplay:product"]?.content?.imdb;
  }
  return {};
};

export const getMovieDetails = (url: string): Promise<IMDBDetails> =>
  axios
    .get(url)
    .then((res) => extractImdbDetails(res.data))
    .catch((err) => {
      if (err.response.status == 404) {
        throw new Exception({ message: "Movie not found", httpCode: 404 });
      }
      logger.error("VIAPLAY_API_ERROR", err.response.data);
      throw new Exception({ message: "Something is wrong" });
    });
