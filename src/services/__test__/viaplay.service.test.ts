import axios from "axios";

import { getMovieDetails } from "../viaplay.service";

describe("Viaplay Service", () => {
  describe("Get Movie Information", () => {
    beforeEach(() => jest.clearAllMocks().resetAllMocks());

    it("When API returns an error", () => {
      const mock = jest.spyOn(axios, "get");
      mock.mockRejectedValueOnce({
        response: {
          status: 500,
        },
      });

      const response = getMovieDetails("awesome movie");

      void expect(response).rejects.toThrow("Something is wrong");
    });

    it("When the movie was not found", () => {
      const mock = jest.spyOn(axios, "get");
      mock.mockRejectedValueOnce({
        response: {
          status: 404,
        },
      });

      const response = getMovieDetails("awesome movie");

      void expect(response).rejects.toThrow("Movie not found");
    });

    it("When the API returns a valid response", () => {
      const mock = jest.spyOn(axios, "get");
      const imdb = {
        id: "string",
        rating: "string",
        votes: "string",
        url: "string",
      };

      mock.mockResolvedValueOnce({
        data: {
          _embedded: {
            "viaplay:blocks": [
              {
                _embedded: {
                  "viaplay:product": {
                    content: { imdb },
                  },
                },
              },
            ],
          },
        },
      });

      const response = getMovieDetails("awesome movie");

      void expect(response).resolves.toBe(imdb);
    });
  });
});
