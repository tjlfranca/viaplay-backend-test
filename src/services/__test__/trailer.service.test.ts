import axios from "axios";

import { getTrailersFromImdb } from "../trailer.service";

describe("Trailer Service", () => {
  describe("Get videos from IMDB info", () => {
    beforeEach(() => jest.clearAllMocks().resetAllMocks());

    it("When API returns an error", () => {
      const mock = jest.spyOn(axios, "get");
      mock.mockRejectedValueOnce({
        response: {
          status: 500,
        },
      });

      const response = getTrailersFromImdb({});

      void expect(response).rejects.toThrow("Something is wrong");
    });

    it("When the movie was not found", () => {
      const mock = jest.spyOn(axios, "get");
      mock.mockRejectedValueOnce({
        response: {
          status: 404,
        },
      });

      const response = getTrailersFromImdb({});

      void expect(response).rejects.toThrow("Movie not found");
    });

    it("When the API returns a valid response", () => {
      const mock = jest.spyOn(axios, "get");

      mock.mockResolvedValueOnce({
        data: {
          results: [
            { site: "YouTube", key: "xyz", type: "Trailer" },
            { site: "InvalidSite", key: "xyz2", type: "Trailer" },
          ],
        },
      });

      const response = getTrailersFromImdb({});

      void expect(response).resolves.toStrictEqual([
        { url: `https://www.youtube.com/watch?v=xyz`, type: "Trailer" },
      ]);
    });
  });
});
