import axios from "axios";

import { Exception } from "../exception";
import { logger } from "../logger";
import { IMDBDetails } from "./viaplay.service";

export const getTrailersFromImdb = (imdb: IMDBDetails) =>
  axios
    .get(
      `https://api.themoviedb.org/3/movie/${imdb.id}/videos?api_key=${process.env.TMDB_API_KEY}&language=en-US`,
    )
    .then((response) => {
      return response?.data?.results
        .filter((r) => r.site === "YouTube" && r.type === "Trailer")
        .map((r) => ({ url: `https://www.youtube.com/watch?v=${r.key}`, type: r.type }));
    })
    .catch((err) => {
      if (err.response.status == 404) {
        throw new Exception({ message: "Movie not found", httpCode: 404 });
      }
      logger.error("TMDB_API_ERROR", err.response.data);
      throw new Exception({ message: "Something is wrong" });
    });
