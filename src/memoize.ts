export const memoizeAsync = (func: (...args) => any, options?: { ignore: boolean }) => {
  const memoized = async (...args) => {
    const key = JSON.stringify(args);
    const cache = memoized.cache;

    if (cache.has(key)) {
      return cache.get(key);
    }
    const result = await func.apply(this, args);
    if (options?.ignore === true) {
      return result;
    }
    memoized.cache = cache.set(key, result) || cache;
    return result;
  };
  memoized.cache = new Map();
  return memoized;
};
