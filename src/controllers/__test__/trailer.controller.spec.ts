import request from "supertest";

import { app } from "../../server";
import * as trailerService from "../../services/trailer.service";
import * as viaplayService from "../../services/viaplay.service";

describe("Trailer Controller", () => {
  describe("GET /trailer", () => {
    it("Without body", (done) => {
      request(app).get("/trailer").query({}).expect(400, done);
    });

    it("With a invalid URL", (done) => {
      request(app).get("/trailer").query({ url: "test" }).expect(400, done);
    });

    it("With a valid payload", async () => {
      const spy = jest.spyOn(viaplayService, "getMovieDetails");
      spy.mockResolvedValueOnce({ id: "" });

      const spyTrailer = jest.spyOn(trailerService, "getTrailersFromImdb");
      spyTrailer.mockResolvedValueOnce([{ url: "awesome", type: "awesome" }]);

      await request(app)
        .get("/trailer")
        .query({ url: "https://content.viaplay.se/pc-se/film/awesome-film" })
        .expect(200);

      expect(spy).toBeCalled();
      expect(spyTrailer).toBeCalled();
    });

    it("When the movie was not found", async () => {
      const spy = jest.spyOn(viaplayService, "getMovieDetails");
      spy.mockRejectedValueOnce({ message: "Movie not found", httpCode: 404 });

      await request(app)
        .get("/trailer")
        .query({ url: "https://content.viaplay.se/pc-se/film/awesome-fidlm-2" })
        .expect(404);

      expect(spy).toBeCalled();
    });
  });
});
