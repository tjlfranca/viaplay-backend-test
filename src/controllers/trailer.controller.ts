import { Router } from "express";
import Joi from "joi";

import { memoizeAsync } from "../memoize";
import { validateQs } from "../middlewares/validator.middleware";
import { getTrailersFromImdb } from "../services/trailer.service";
import { getMovieDetails } from "../services/viaplay.service";

export const trailerController = () =>
  Router().get(
    "/",
    validateQs(
      Joi.object({
        url: Joi.string()
          .uri()
          .pattern(/(?:(?:https):\/\/)?(?:content.viaplay.se)\/([A-Za-z0-9-_]+)/im)
          .required(),
      }),
    ),
    getTrailer,
  );

const getTrailerFromUrl = memoizeAsync((url) => getMovieDetails(url).then(getTrailersFromImdb));

const getTrailer = (req, res, next) =>
  getTrailerFromUrl(req.query.url)
    .then((data) => res.json(data))
    .catch(next);
