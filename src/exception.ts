export class Exception extends Error {
  httpCode: number;

  constructor(error: { message: string; httpCode?: number }) {
    super(error.message);
    this.httpCode = error.httpCode || 500;
  }
}
