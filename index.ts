import { config } from "dotenv";
import { resolve } from "path";
config({ path: resolve(__dirname, ".env") });

import { app } from "./src/server";

app.listen(process.env.PORT || 3000, () => {
  console.log(`App is runing on port 3000`);
});
