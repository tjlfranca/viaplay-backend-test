## Viaplay Coding Test

## Dependencies
You must need a valid [themoviedb](https://www.themoviedb.org/documentation/api) API KEY

## Technologies Used
1. Typescript
2. Express
3. Joi
4. Axios

## Setup

First, install all dependencies
```bash
yarn
```

then, create you `.env` file (based on `.env.example`)

finnaly, you can run this command to up your server:
```bash
yarn start:dev
```

## Testing The API
With the default settings the API endpoint will be http://localhost:3000/trailer/. You can test the API making a get request like that:
```bash
curl http://localhost:3000/trailer?url=https://content.viaplay.se/pc-se/film/arrival-2016
```

## Caching
The project is using a memory-based cache (using a movie URL as a key). [Memoize](https://en.wikipedia.org/wiki/Memoization) strategy is used for caching.

This is not a reliable production solution. For a production environment, a solution like Redis is more appropriate.

## Testing
To run all tests:
```bash
yarn start:dev
```

